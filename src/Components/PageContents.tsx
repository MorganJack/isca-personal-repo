import { Fragment } from "react";

function pageContents() {
  return (
    <>
        <div className="container-fluid bg-danger text-wrap justify-content-center">
          <h2 className="fs-2 text-white text-center">
            <strong>The ISCA Mission</strong>
          </h2>
          <p className="fw-normal fs-6 text-white text-center">
            The International Society for Computers and Their Applications,
            Inc., promotes the advancement of science and engineering in the
            area of computers and their applications, and disseminates this
            technology throughout the world.
          </p>
        </div>
    </>
  );
}

export default pageContents;
