import { Fragment } from "react";

function GutterLeft() {
  return (
    <>
      <div className="container-fluid text-center">
        <div className="row bg-danger">
        <div className="col">
          <div className="p-3 bg-danger">
          <p className="fw-normal fs-6 text-white">Listed below are recently submitted papers
          </p>
          </div>
          </div>
          <table className="table">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Title</th>
      <th scope="col">Last Name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>The Code Breakers: Exploring the Foundations of Computer Science</td>
      <td>Anderson</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Algorithms Unleashed: A Journey into Computational Thinking</td>
      <td>Smith</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Digital Horizons: Navigating the World of Computer Science</td>
      <td>Johnson</td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>Bits and Bytes: Understanding the Essence of Computing</td>
      <td>Patel</td>
    </tr>
    <tr>
      <th scope="row">5</th>
      <td>The Logic of Machines: Mastering Computer Science Principles</td>
      <td>Garcia</td>
    </tr>
    <tr>
      <th scope="row">6</th>
      <td>Programming Paradigms: A Comprehensive Guide to Software Engineering</td>
      <td>Lee</td>
    </tr>
    <tr>
      <th scope="row">7</th>
      <td>Cybernetic Explorations: Discovering the Heart of Computer Science</td>
      <td>Martinez</td>
    </tr>
    <tr>
      <th scope="row">8</th>
      <td>Data Dynamics: Unraveling the Secrets of Big Data and Analytics</td>
      <td>Williams</td>
    </tr>
  </tbody>
</table>
          
          <div className="col">
          <div className="p-3 bg-danger text-white">
          </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default GutterLeft;
