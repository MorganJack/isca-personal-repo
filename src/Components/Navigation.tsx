import { Fragment } from "react";

function Navigation() {
  return (
    <>
      <nav className="navbar navbar-expand-md bg-danger">
        <div className="container-fluid">
          <a className="navbar-brand text-white" href="#">
            ISCA
          </a>

          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarMenu"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarMenu">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link text-white" href="#Home">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#Journals">
                  Jounals
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#Conferences">
                  Conferences
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#About">
                  About
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navigation;
