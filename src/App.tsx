import React from "react";
import Navigation from "./Components/Navigation";
import PageContents from "./Components/PageContents";
import GutterLeft from "./Components/GutterLeft";

function app()
{
  return(
    <>
      <Navigation />
      <div className="container-fluid bg-white">
        <div className="row">
        </div>
      </div>
      <PageContents />
      <div>
      </div>
      <GutterLeft />
    </>
  )
}

export default app;